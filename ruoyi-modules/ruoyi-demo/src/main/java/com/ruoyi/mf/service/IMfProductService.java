package com.ruoyi.mf.service;

import java.util.List;
import com.ruoyi.mf.domain.MfProduct;
import com.ruoyi.mf.domain.vo.MfProductVo;
import com.ruoyi.mf.domain.bo.MfProductBo;
import com.ruoyi.common.orm.core.service.IBaseService;

/**
 * 产品树Service接口
 *
 * @author 数据小王子
 * 2024-01-06
 */
public interface IMfProductService extends IBaseService<MfProduct>
{
    /**
     * 查询产品树
     *
     * @param productId 产品树主键
     * @return 产品树
     */
    MfProductVo selectById(Long productId);

    /**
     * 查询产品树列表
     *
     * @param mfProductBo 产品树Bo
     * @return 产品树集合
     */
    List<MfProductVo> selectList(MfProductBo mfProductBo);


    /**
     * 新增产品树
     *
     * @param mfProductBo 产品树Bo
     * @return 结果:true 操作成功，false 操作失败
     */
    boolean insert(MfProductBo mfProductBo);

    /**
     * 修改产品树
     *
     * @param mfProductBo 产品树Bo
     * @return 结果:true 更新成功，false 更新失败
     */
    boolean update(MfProductBo mfProductBo);

    /**
     * 批量删除产品树
     *
     * @param productIds 需要删除的产品树主键集合
     * @return 结果:true 删除成功，false 删除失败
     */
    boolean deleteByIds(Long[] productIds);

}
