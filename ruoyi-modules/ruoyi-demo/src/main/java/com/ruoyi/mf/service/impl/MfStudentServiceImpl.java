package com.ruoyi.mf.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import cn.hutool.core.util.ObjectUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.ruoyi.common.core.utils.MapstructUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.orm.core.page.PageQuery;
import com.ruoyi.common.orm.core.page.TableDataInfo;
import com.ruoyi.common.orm.core.service.impl.BaseServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.mf.mapper.MfStudentMapper;
import com.ruoyi.mf.domain.MfStudent;
import com.ruoyi.mf.domain.bo.MfStudentBo;
import com.ruoyi.mf.domain.vo.MfStudentVo;
import com.ruoyi.mf.service.IMfStudentService;
import static com.ruoyi.mf.domain.table.MfStudentTableDef.MF_STUDENT;

/**
 * 学生信息表Service业务层处理
 *
 * @author 数据小王子
 * 2024-01-05
 */
@Service
public class MfStudentServiceImpl extends BaseServiceImpl<MfStudentMapper, MfStudent> implements IMfStudentService
{
    @Resource
    private MfStudentMapper mfStudentMapper;

    @Override
    public QueryWrapper query() {
        return super.query().from(MF_STUDENT);
    }

    private QueryWrapper buildQueryWrapper(MfStudentBo mfStudentBo) {
        QueryWrapper queryWrapper = super.buildBaseQueryWrapper();
        queryWrapper.and(MF_STUDENT.STUDENT_NAME.like(mfStudentBo.getStudentName()));
        queryWrapper.and(MF_STUDENT.STUDENT_STATUS.eq(mfStudentBo.getStudentStatus()));

        return queryWrapper;
    }

    /**
     * 查询学生信息表
     *
     * @param studentId 学生信息表主键
     * @return 学生信息表
     */
    @Override
    public MfStudentVo selectById(Long studentId)
    {
        return this.getOneAs(query().where(MF_STUDENT.STUDENT_ID.eq(studentId)), MfStudentVo.class);

    }

    /**
     * 查询学生信息表列表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 学生信息表集合
     */
    @Override
    public List<MfStudentVo> selectList(MfStudentBo mfStudentBo)
    {
        QueryWrapper queryWrapper = buildQueryWrapper(mfStudentBo);
        return this.listAs(queryWrapper, MfStudentVo.class);
    }

    /**
     * 分页查询学生信息表列表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 分页学生信息表集合
     */
    //@UseDataSource("ds2") //多数据源演示
    @Override
    public TableDataInfo<MfStudentVo> selectPage(MfStudentBo mfStudentBo)
    {
        QueryWrapper queryWrapper = buildQueryWrapper(mfStudentBo);
        Page<MfStudentVo> page = this.pageAs(PageQuery.build(), queryWrapper, MfStudentVo.class);
        return TableDataInfo.build(page);
    }

    /**
     * 新增学生信息表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 结果:true 操作成功，false 操作失败
     */
    @Override
    public boolean insert(MfStudentBo mfStudentBo)
    {
        MfStudent mfStudent = MapstructUtils.convert(mfStudentBo, MfStudent.class);

        return this.save(mfStudent);//使用全局配置的雪花算法主键生成器生成ID值
    }

    /**
     * 修改学生信息表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 结果:true 更新成功，false 更新失败
     */
    @Override
    public boolean update(MfStudentBo mfStudentBo)
    {
        MfStudent mfStudent = MapstructUtils.convert(mfStudentBo, MfStudent.class);
        if(ObjectUtil.isNotNull(mfStudent) && ObjectUtil.isNotNull(mfStudent.getStudentId())) {
            boolean updated = this.updateById(mfStudent);
            return updated;
        }
        return false;
    }

    /**
     * 批量删除学生信息表
     *
     * @param studentIds 需要删除的学生信息表主键集合
     * @return 结果:true 删除成功，false 删除失败
     */
    @Transactional
    @Override
    public boolean deleteByIds(Long[] studentIds)
    {
        return this.removeByIds(Arrays.asList(studentIds));
    }

}
