package com.ruoyi.mf.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import cn.hutool.core.util.ObjectUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.ruoyi.common.core.utils.MapstructUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.orm.core.page.PageQuery;
import com.ruoyi.common.orm.core.page.TableDataInfo;
import com.ruoyi.common.orm.core.service.impl.BaseServiceImpl;
import com.ruoyi.common.core.utils.DateUtils;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.mf.mapper.MfProductMapper;
import com.ruoyi.mf.domain.MfProduct;
import com.ruoyi.mf.domain.bo.MfProductBo;
import com.ruoyi.mf.domain.vo.MfProductVo;
import com.ruoyi.mf.service.IMfProductService;
import static com.ruoyi.mf.domain.table.MfProductTableDef.MF_PRODUCT;

/**
 * 产品树Service业务层处理
 *
 * @author 数据小王子
 * 2024-01-06
 */
@Service
public class MfProductServiceImpl extends BaseServiceImpl<MfProductMapper, MfProduct> implements IMfProductService
{
    @Resource
    private MfProductMapper mfProductMapper;

    @Override
    public QueryWrapper query() {
        return super.query().from(MF_PRODUCT);
    }

    private QueryWrapper buildQueryWrapper(MfProductBo mfProductBo) {
        QueryWrapper queryWrapper = super.buildBaseQueryWrapper();
        queryWrapper.and(MF_PRODUCT.PRODUCT_NAME.like(mfProductBo.getProductName()));
        queryWrapper.and(MF_PRODUCT.STATUS.eq(mfProductBo.getStatus()));
        queryWrapper.orderBy(MF_PRODUCT.ORDER_NUM.asc());

        return queryWrapper;
    }

    /**
     * 查询产品树
     *
     * @param productId 产品树主键
     * @return 产品树
     */
    @Override
    public MfProductVo selectById(Long productId)
    {
        return this.getOneAs(query().where(MF_PRODUCT.PRODUCT_ID.eq(productId)), MfProductVo.class);

    }

    /**
     * 查询产品树列表
     *
     * @param mfProductBo 产品树Bo
     * @return 产品树集合
     */
    @Override
    public List<MfProductVo> selectList(MfProductBo mfProductBo)
    {
        QueryWrapper queryWrapper = buildQueryWrapper(mfProductBo);
        return this.listAs(queryWrapper, MfProductVo.class);
    }


    /**
     * 新增产品树
     *
     * @param mfProductBo 产品树Bo
     * @return 结果:true 操作成功，false 操作失败
     */
    @Override
    public boolean insert(MfProductBo mfProductBo)
    {
        MfProduct mfProduct = MapstructUtils.convert(mfProductBo, MfProduct.class);

        return this.save(mfProduct);//使用全局配置的雪花算法主键生成器生成ID值
    }

    /**
     * 修改产品树
     *
     * @param mfProductBo 产品树Bo
     * @return 结果:true 更新成功，false 更新失败
     */
    @Override
    public boolean update(MfProductBo mfProductBo)
    {
        MfProduct mfProduct = MapstructUtils.convert(mfProductBo, MfProduct.class);
        if(ObjectUtil.isNotNull(mfProduct) && ObjectUtil.isNotNull(mfProduct.getProductId())) {
            boolean updated = this.updateById(mfProduct);
            return updated;
        }
        return false;
    }

    /**
     * 批量删除产品树
     *
     * @param productIds 需要删除的产品树主键集合
     * @return 结果:true 删除成功，false 删除失败
     */
    @Transactional
    @Override
    public boolean deleteByIds(Long[] productIds)
    {
        return this.removeByIds(Arrays.asList(productIds));
    }

}
