package com.ruoyi.mf.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.core.core.domain.R;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.web.annotation.RepeatSubmit;
import com.ruoyi.common.web.core.BaseController;
import jakarta.annotation.Resource;
import com.ruoyi.mf.domain.vo.MfStudentVo;
import com.ruoyi.mf.domain.bo.MfStudentBo;
import com.ruoyi.mf.service.IMfStudentService;

import com.ruoyi.common.orm.core.page.TableDataInfo;

/**
 * 学生信息表Controller
 *
 * @author 数据小王子
 * 2024-01-05
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/mf/student")
public class MfStudentController extends BaseController
{
    @Resource
    private IMfStudentService mfStudentService;

    /**
     * 查询学生信息表列表
     */
    @SaCheckPermission("mf:student:list")
    @GetMapping("/list")
    public TableDataInfo<MfStudentVo> list(MfStudentBo mfStudentBo)
    {
        return mfStudentService.selectPage(mfStudentBo);
    }

    /**
     * 导出学生信息表列表
     */
    @SaCheckPermission("mf:student:export")
    @Log(title = "学生信息表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MfStudentBo mfStudentBo)
    {
        List<MfStudentVo> list = mfStudentService.selectList(mfStudentBo);
        ExcelUtil.exportExcel(list, "学生信息表", MfStudentVo.class, response);
    }

    /**
     * 获取学生信息表详细信息
     */
    @SaCheckPermission("mf:student:query")
    @GetMapping(value = "/{studentId}")
    public R<MfStudentVo> getInfo(@PathVariable Long studentId)
    {
        return R.ok(mfStudentService.selectById(studentId));
    }

    /**
     * 新增学生信息表
     */
    @SaCheckPermission("mf:student:add")
    @Log(title = "学生信息表", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping
    public R<Void> add(@Validated @RequestBody MfStudentBo mfStudentBo)
    {
        boolean inserted = mfStudentService.insert(mfStudentBo);
        if (!inserted) {
            return R.fail("新增学生信息表记录失败！");
        }
        return R.ok();
    }

    /**
     * 修改学生信息表
     */
    @SaCheckPermission("mf:student:edit")
    @Log(title = "学生信息表", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping
    public R<Void> edit(@Validated @RequestBody MfStudentBo mfStudentBo)
    {
        Boolean updated = mfStudentService.update(mfStudentBo);
        if (!updated) {
            return R.fail("修改学生信息表记录失败!");
        }
        return R.ok();
    }

    /**
     * 删除学生信息表
     */
    @SaCheckPermission("mf:student:remove")
    @Log(title = "学生信息表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{studentIds}")
    public R<Void> remove(@PathVariable Long[] studentIds)
    {
        boolean deleted = mfStudentService.deleteByIds(studentIds);
        if (!deleted) {
            return R.fail("删除学生信息表记录失败!");
        }
        return R.ok();
    }
}
