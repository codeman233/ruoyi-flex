package com.ruoyi.mf.domain.bo;

import com.ruoyi.mf.domain.MfProduct;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;
import com.ruoyi.common.orm.core.domain.TreeEntity;

/**
 * 产品树业务对象 mf_product
 *
 * @author 数据小王子
 * @date 2024-01-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = MfProduct.class, reverseConvertGenerate = false)
public class MfProductBo extends TreeEntity
{

    /**
     * 产品id
     */
    private Long productId;

    /**
     * 产品名称
     */
    @NotBlank(message = "产品名称不能为空")
    private String productName;

    /**
     * 产品状态（0正常 1停用）
     */
    @NotBlank(message = "产品状态（0正常 1停用）不能为空")
    private String status;

}
