package com.ruoyi.common.orm.core.domain;

import com.mybatisflex.annotation.Column;
import lombok.Data;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Tree基类
 *
 * @author ruoyi
 * @author 数据小王子
 */
@Data
public class TreeEntity extends BaseEntity
{
    @Serial
    private static final long serialVersionUID = 1L;

    /** 父菜单名称 */
    @Column(ignore = true)
    private String parentName;

    /** 父菜单ID */
    private Long parentId;

    /** 显示顺序 */
    private Integer orderNum;

    /** 祖级列表 */
    @Column(ignore = true)
    private String ancestors;

    /** 子部门 */
    @Column(ignore = true)
    private List<Object> children = new ArrayList<>();
}
